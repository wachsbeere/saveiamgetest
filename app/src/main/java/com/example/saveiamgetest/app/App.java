package com.example.saveiamgetest.app;


import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.os.StrictMode;

import java.io.File;

public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    private static File sCacheDir;
    private static ContentResolver sContentResolver;
    @SuppressLint("StaticFieldLeak")
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        sCacheDir = getExternalCacheDir();
        sContentResolver = getContentResolver();
        instance = this;

        //7.0 以上照片崩溃？
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        builder.detectFileUriExposure();
//        StrictMode.setVmPolicy(builder.build());
    }

    public static Context getmContext() {
        return mContext;
    }

    public static File getsCacheDir() {
        return sCacheDir;
    }

    public static ContentResolver getsContentResolver() {
        return sContentResolver;
    }

    public static App getInstance() {
        return instance;
    }
}
