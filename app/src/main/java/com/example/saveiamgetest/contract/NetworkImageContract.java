package com.example.saveiamgetest.contract;

import com.example.saveiamgetest.bean.NetworkImage;

import java.util.List;

public interface NetworkImageContract {

    interface IView {

        void getImageByPage(int page);

        void showImagesInaPage(List<NetworkImage> imageList);

        void onError(String msg);

    }

    interface IPresenter {

        void getImageByPage(int page);

        void showImagesInaPaage(List<NetworkImage> imageList);

        void onError(String msg);


    }

    interface IModel {

        void getImageByPage(int page);

    }
}
