package com.example.saveiamgetest.fragment;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.example.saveiamgetest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReadGalleryMaxFragment extends DialogFragment {

    private View mRootView;
    private Context mContext;
    private static String mImageUrl;

    @BindView(R.id.image_gallery_max)
    ImageView maxImage;

    public static ReadGalleryMaxFragment getInstance(String url) {
        mImageUrl = url;
        return new ReadGalleryMaxFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
//        mRootView = inflater.inflate(R.layout.fragment_gallery_max, container, false);
        mRootView = inflater.inflate(R.layout.fragment_gallery_max, container);
        ButterKnife.bind(this, mRootView);

        initViews(mRootView);

        Window mWindow = getDialog().getWindow();
        mWindow.requestFeature(Window.FEATURE_NO_TITLE);

        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
    }

    private void initViews(View view) {

        Bitmap bitmap = BitmapFactory.decodeFile(mImageUrl);
        maxImage.setImageBitmap(bitmap);

    }
}