package com.example.saveiamgetest.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.saveiamgetest.R;
import com.example.saveiamgetest.adapter.NetworkImageRecyclerViewAdapter;
import com.example.saveiamgetest.bean.NetworkImage;
import com.example.saveiamgetest.contract.NetworkImageContract;
import com.example.saveiamgetest.listener.OnClickRecyclerViewListener;
import com.example.saveiamgetest.presenter.NetworkImagePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NetworkImageFragment extends Fragment implements NetworkImageContract.IView {

    @BindView(R.id.ibtn_beck)
    ImageButton ibtn_back;
    @BindView(R.id.ibtn_forward)
    ImageButton ibtn_forward;
    @BindView(R.id.network_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_page)
    TextView tv_page;

    private View mRootView;
    private Context mContext;
    protected Activity mActivity;
    private List<NetworkImage> networkImageList;
    private NetworkImageRecyclerViewAdapter adapter;

    private NetworkImagePresenter presenter;

    private static int currPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_network_image, container, false);
        ButterKnife.bind(this, mRootView);
        initView();
        initListener();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        this.mActivity = (Activity) context;
    }

    private void initView() {
        ibtn_back.setOnClickListener(v -> back());
        ibtn_forward.setOnClickListener(v -> forward());
        networkImageList = new ArrayList<>();
        adapter = new NetworkImageRecyclerViewAdapter();
        adapter.updateData(networkImageList);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);


        presenter = new NetworkImagePresenter(this);
//        presenter.getImageByPage(currPage);
        getImageByPage(currPage);
    }

    private OnClickRecyclerViewListener onClickRecyclerViewListener = new OnClickRecyclerViewListener() {
        @Override
        public void onItemClick(int position) {
            showBigImage(position);
        }

        @Override
        public boolean onItemLongClick(int position) {
            return false;
        }

        @Override
        public void onFooterViewClick() {

        }
    };

    private void initListener() {
        adapter.setOnRecyclerViewListener(onClickRecyclerViewListener);
    }

    private void back() {
        if (currPage == 0) {
            Toast.makeText(mContext, "已是第一页", Toast.LENGTH_SHORT).show();
        } else {
            currPage -= 1;
            getImageByPage(currPage);
        }
    }

    private void forward() {
        currPage += 1;
        getImageByPage(currPage);
    }


    private void showBigImage(int position) {
        NetworkImageMaxFragment.getInstance(networkImageList.get(position).getTitle(), networkImageList.get(position).getUrl()).show(getFragmentManager(), "dialog");
    }

    @Override
    public void getImageByPage(int page) {
        presenter.getImageByPage(currPage);
    }

    @Override
    public void showImagesInaPage(List<NetworkImage> imageList) {
        if (imageList == null) {
            Toast.makeText(mContext, "到达尽头", Toast.LENGTH_SHORT).show();
            if (networkImageList != null) {
                currPage -= 1;
            }
        } else {
            networkImageList.clear();
            networkImageList.addAll(imageList);
            adapter.notifyDataSetChanged();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_page.setText("第 " + (currPage + 1) + " 页");
                }
            });
        }
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(mContext, "error " + msg, Toast.LENGTH_SHORT).show();
    }
}