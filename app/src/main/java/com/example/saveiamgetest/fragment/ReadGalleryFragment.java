package com.example.saveiamgetest.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.saveiamgetest.R;
import com.example.saveiamgetest.adapter.GridViewAdapter;
import com.example.saveiamgetest.bean.GalleryItem;
import com.example.saveiamgetest.listener.OnClickRecyclerViewListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReadGalleryFragment extends Fragment {

    @BindView(R.id.gridview)
    GridView gridView;

    private View mRootView;
    private Context mContext;
    protected Activity mActivity;
    private List<GalleryItem> galleryItemList;
    private GridViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_read_gallery, container, false);
        ButterKnife.bind(this, mRootView);
        initView();
        initData();
        initListener();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        this.mActivity = (Activity) context;
    }

    private void initView() {

    }

    private void initData() {
        galleryItemList = new ArrayList<>();
    }

    private void initGallery() {

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        ContentResolver contentResolver = mActivity.getContentResolver();

        String selection = MediaStore.Images.Media.MIME_TYPE + "=\"image/jpeg\" or " +
                MediaStore.Images.Media.MIME_TYPE + "=\"image/png\"";

        String sortOrder = MediaStore.Images.Media.DATE_MODIFIED + " desc";     // 获取jpeg和png格式的文件，并且按照时间进行倒序

        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);
        if (cursor != null) {
            galleryItemList.clear();

            while (cursor.moveToNext()) {

                GalleryItem galleryItem = new GalleryItem();
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                System.out.println("initGallery : " + path);
                galleryItem.setPath(path);

                galleryItemList.add(galleryItem);
            }

            String[] objects = new String[galleryItemList.size()];

            for (int j = 0; j < galleryItemList.size(); j++) {
                objects[j] = galleryItemList.get(j).getPath();
            }
            adapter = new GridViewAdapter(mContext, mActivity, 0, objects, gridView, galleryItemList);
            gridView.setAdapter(adapter);
        }
    }

    private void getAllImages(){
        if (ContextCompat.checkSelfPermission(mActivity.getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mActivity.getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            initGallery();
        }
        else{
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }


    private OnClickRecyclerViewListener onClickRecyclerViewListener = new OnClickRecyclerViewListener() {
        @Override
        public void onItemClick(int position) {
            ReadGalleryMaxFragment.getInstance(galleryItemList.get(position).getPath()).show(getFragmentManager(), "dialog");
        }

        @Override
        public boolean onItemLongClick(int position) {
            return false;
        }

        @Override
        public void onFooterViewClick() {

        }
    };

    private void initListener() {
        adapter.setOnRecyclerViewListener(onClickRecyclerViewListener);
    }


}
