package com.example.saveiamgetest.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.example.saveiamgetest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NetworkImageMaxFragment extends DialogFragment {

    private View mRootView;
    private Context mContext;
    private static String mTitle;
    private static String mImageUrl;

    @BindView(R.id.image_network_max)
    ImageView maxImage;

    public static NetworkImageMaxFragment getInstance(String title, String url) {
        mTitle = title;
        mImageUrl = url;
        return new NetworkImageMaxFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
        mRootView = inflater.inflate(R.layout.fragment_network_max, container, false);
        ButterKnife.bind(this, mRootView);

        initViews(mRootView);
        return mRootView;
    }

    private void initViews(View view) {

        Glide.with(view).load(mImageUrl).into(maxImage);
    }
}
