package com.example.saveiamgetest.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.saveiamgetest.db.MySQLiteOperator;
import com.example.saveiamgetest.R;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReadImageFragment extends Fragment {

    @BindView(R.id.btn_readImage)
    Button btn_readImage;

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.btn_save)
    Button btn_save;

    private View mRootView;
    private Context mContext;
    private Activity mActivity;
    private MySQLiteOperator mySQLiteOperator;

    public static final int GALLERY = 2;        // 相册
    public static final String IMAGE_UNSPECIFIED = "image/*";

    private static Bitmap bitmapTemp;
    private static String pathTemp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_readimage, container, false);
        ButterKnife.bind(this, mRootView);

        btn_readImage.setOnClickListener(v -> readImage());
        btn_save.setOnClickListener(v -> save());
        return mRootView;
    }

    @OnClick(R.id.btn_readImage)
    public void readImage() {
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, null);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);

            startActivityForResult(intent, GALLERY);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        this.mActivity = (Activity) context;
        mySQLiteOperator = new MySQLiteOperator(mContext);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if (requestCode == GALLERY) {
            try {
                Uri uri = data.getData();
                String imagePath = uri.getPath();
                if (imagePath != null) {
                    System.out.println("ReadImageFragment onActivityResult imagePath ：" + imagePath);
                }
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), uri);
                image.setImageBitmap(bitmap);
                bitmapTemp = bitmap;
                pathTemp = imagePath;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void save() {
        if (bitmapTemp != null && pathTemp != null) {
            if (!mySQLiteOperator.isPhotoExist(pathTemp)) {
                mySQLiteOperator.saveImage(bitmapTemp, pathTemp);
                Toast.makeText(mContext, "保存成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "图片已存在", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
