package com.example.saveiamgetest.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.saveiamgetest.R;
import com.example.saveiamgetest.adapter.ShowImageRecyclerViewAdapter;
import com.example.saveiamgetest.bean.ImageItem;
import com.example.saveiamgetest.db.MySQLiteOperator;
import com.example.saveiamgetest.listener.OnClickRecyclerViewListener;
import com.example.saveiamgetest.util.ShareUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShareImageFragment extends Fragment {

    @BindView(R.id.btn_getImage)
    Button btn_getImage;
    @BindView(R.id.listView)
    RecyclerView recyclerView;

    private View mRootView;
    private Context mContext;
    protected Activity mActivity;
    private MySQLiteOperator mySQLiteOperator;
    private List<ImageItem> imageItemList;
    private ShowImageRecyclerViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_shareimage, container, false);
        ButterKnife.bind(this, mRootView);
        initView();
        initListener();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        this.mActivity = (Activity) context;
    }

    private void initView() {
        mySQLiteOperator = new MySQLiteOperator(mContext);
        btn_getImage.setOnClickListener(v -> getImage());
        imageItemList = new ArrayList<>();
        adapter = new ShowImageRecyclerViewAdapter();
        adapter.updateData(imageItemList);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    private OnClickRecyclerViewListener onClickRecyclerViewListener = new OnClickRecyclerViewListener() {
        @Override
        public void onItemClick(int position) {
            onForward(imageItemList.get(position).getImage());
        }

        @Override
        public boolean onItemLongClick(int position) {
            return false;
        }

        @Override
        public void onFooterViewClick() {

        }
    };

    private void initListener() {
        adapter.setOnRecyclerViewListener(onClickRecyclerViewListener);
    }

    private void getImage() {
        imageItemList.clear();
        imageItemList.addAll(mySQLiteOperator.readImageList());
        adapter.notifyDataSetChanged();
    }

    public void onForward(Bitmap bitmap) {
        Toast.makeText(mContext, "生成图片中...", Toast.LENGTH_SHORT).show();
        new Thread(() -> {
            Uri uri = ShareUtil.saveBitmap(bitmap, "share");
            ShareUtil.share(getFragmentManager(), "分享图片", uri);
        }).start();
    }

}
