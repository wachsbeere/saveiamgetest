package com.example.saveiamgetest.adapter;

import android.util.Log;
import android.util.SparseArray;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.saveiamgetest.fragment.NetworkImageFragment;
import com.example.saveiamgetest.fragment.ReadGalleryFragment;
import com.example.saveiamgetest.fragment.ReadImageFragment;
import com.example.saveiamgetest.fragment.ShareImageFragment;


public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private int num;
    private SparseArray<Fragment> mFragmentHashMap = new SparseArray<>();
    private String[] mTitleList = new String[]{"读取相片", "分享相片", "本地服务器照片", "本地相册"};

    public MyFragmentPagerAdapter(FragmentManager fm, int num) {
        super(fm);
        this.num = num;
    }

    @Override
    public Fragment getItem(int position) {
//        return createFragment(position);
        Fragment fragment = mFragmentHashMap.get(position);
        if (fragment == null) {
            switch (position) {
                case 0:
                    fragment = new ReadImageFragment();
                    break;
                case 1:
                    fragment = new ShareImageFragment();
                    break;
                case 2:
                    fragment = new NetworkImageFragment();
                    break;
                case 3:
                    fragment = new ReadGalleryFragment();
                    break;
            }
            mFragmentHashMap.put(position, fragment);
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList[position];
    }


    @Override
    public int getCount() {
        return num;
    }
}