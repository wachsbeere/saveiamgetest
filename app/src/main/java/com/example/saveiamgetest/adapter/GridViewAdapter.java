package com.example.saveiamgetest.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.saveiamgetest.R;
import com.example.saveiamgetest.app.App;
import com.example.saveiamgetest.bean.GalleryItem;
import com.example.saveiamgetest.listener.OnClickRecyclerViewListener;
import com.example.saveiamgetest.util.GalleryUtil;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class GridViewAdapter extends ArrayAdapter<String> {

    private GridView mGridView;
    private LruCache<String, Bitmap> mLruCache;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private boolean isFirstEnterThisActivity = true;
    private List<GalleryItem> galleryItemList;
    protected OnClickRecyclerViewListener mOnRecyclerViewListener;

    private Context context;
    private Activity activity;

    private static int preCacheNum = 50;

    public GridViewAdapter(Context context, Activity activity, int textViewResourceId, String[] objects, GridView gridView, List<GalleryItem> galleryItemList) {

        super(context, textViewResourceId, objects);
        this.galleryItemList = galleryItemList;
        this.context = context;
        this.activity = activity;
        mGridView = gridView;
        mGridView.setOnScrollListener(new ScrollListenerImpl());
        int maxMemory = (int) Runtime.getRuntime().maxMemory();
        int cacheSize = maxMemory / 2;              // 缓存大小

        mLruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        };
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String path = getItem(position);
        View view;

        if (convertView == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_gallery, null);
            int perWidth = (((WindowManager) (App.getInstance().getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay().getWidth() - GalleryUtil.dip2px(context, 4)) / 3;
            view.setLayoutParams(new GridView.LayoutParams(perWidth, perWidth));
        } else {
            view = convertView;
        }

        view.setOnClickListener(v -> mOnRecyclerViewListener.onItemClick(position));
        ImageView imageView = (ImageView) view.findViewById(R.id.image_item_gallery);
        imageView.setTag(path);         // 为该ImageView设置一个Tag,防止图片错位
        setImageForImageView(path, imageView);
        return view;
    }

    public void setOnRecyclerViewListener(OnClickRecyclerViewListener onRecyclerViewListener) {
        mOnRecyclerViewListener = onRecyclerViewListener;
    }

    private void setImageForImageView(String imagePath, ImageView imageView) {
        Bitmap bitmap = getBitmapFromLruCache(imagePath);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(R.drawable.ic_launcher_background);
        }
    }

    private void addBitmapToLruCache(String key, Bitmap bitmap) {
        if (getBitmapFromLruCache(key) == null) {
            mLruCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromLruCache(String key) {
        return mLruCache.get(key);
    }

    private ThreadPoolExecutor tpe = new ThreadPoolExecutor(2, 5, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private void loadBitmaps(int firstVisibleItem, int visibleItemCount) {
        try {
            for (int i = firstVisibleItem; i < firstVisibleItem + visibleItemCount + preCacheNum; i++) {
                String imagePath = galleryItemList.get(i).getPath();
                Bitmap bitmap = getBitmapFromLruCache(imagePath);
                if (bitmap == null) {

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = 4;
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    options.inJustDecodeBounds = false;

                    ImageView imageView = (ImageView) mGridView.findViewWithTag(imagePath);

                    tpe.execute(() -> {
                        Bitmap bitmap1 = BitmapFactory.decodeFile(imagePath, options);           // 耗时
                        addBitmapToLruCache(imagePath, bitmap1);

                        activity.runOnUiThread(() -> {
                            if (imageView != null && bitmap1 != null) {
                                imageView.setImageBitmap(bitmap1);
                            }
                        });
                    });

                } else {
                    // 依据Tag找到对应的ImageView显示图片
                    ImageView imageView = (ImageView) mGridView.findViewWithTag(imagePath);
                    if (imageView != null && bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ScrollListenerImpl implements AbsListView.OnScrollListener {

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            mFirstVisibleItem = firstVisibleItem;
            mVisibleItemCount = visibleItemCount;
            if (isFirstEnterThisActivity && visibleItemCount > 0) {
                loadBitmaps(firstVisibleItem, visibleItemCount);
                isFirstEnterThisActivity = false;
            }

        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                removeAllThreads();
                loadBitmaps(mFirstVisibleItem, mVisibleItemCount);
            }
        }
    }

    public void removeAllThreads() {
        for (Runnable runnable : tpe.getQueue()) {
            tpe.remove(runnable);
        }
    }
}
