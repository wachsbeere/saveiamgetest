package com.example.saveiamgetest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.saveiamgetest.R;
import com.example.saveiamgetest.bean.ImageItem;


import butterknife.BindView;

public class ShowImageRecyclerViewAdapter extends BaseRvAdapter<ImageItem> {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShowImageItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_show_image, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ShowImageItemViewHolder) holder).bindView(mDataList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.size();
    }

    class ShowImageItemViewHolder extends BaseRvHolder {

        @BindView(R.id.item_show_imageView) ImageView imageView;

        public ShowImageItemViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bindView(ImageItem imageItem) {

            imageView.setImageBitmap(imageItem.getImage());
        }
    }
}



//public class ShowImageRecyclerViewAdapter extends RecyclerView.Adapter<ShowImageRecyclerViewAdapter.ItemViewHolder> {
//
//    private List<ImageItem> mList;
//    private Context mContext;
//    private OnClickImageItemListener mOnClickImageItemListener = null;
//
//    public ShowImageRecyclerViewAdapter(List<ImageItem> list, Context context) {
//        mList = list;
//        mContext = context;
//    }
//
//    @NonNull
//    @Override
//    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(mContext).inflate(R.layout.item_show_image, parent, false);
//        return new ItemViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ItemViewHolder holder, @SuppressLint("RecyclerView") final int position) {
//        holder.image.setImageBitmap(mList.get(position).getImage());
//        if (mOnClickImageItemListener != null) {
//            holder.itemView.setOnClickListener(view -> mOnClickImageItemListener.OnClick(position));
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return mList == null ? 0 : mList.size();
//    }
//
//    class ItemViewHolder extends RecyclerView.ViewHolder {
//
//        private ImageView image;
//
//        ItemViewHolder(View itemView) {
//            super(itemView);
//            image = itemView.findViewById(R.id.item_show_imageView);
//        }
//    }
//
//    public void setOnClickImageItemListener(OnClickImageItemListener onClickImageItemListener) {
//        mOnClickImageItemListener = onClickImageItemListener;
//    }
//
//    public interface OnClickImageItemListener {
//        void OnClick(int position);
//    }
//}