package com.example.saveiamgetest.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.saveiamgetest.R;
import com.example.saveiamgetest.bean.NetworkImage;

import butterknife.BindView;

public class NetworkImageRecyclerViewAdapter extends BaseRvAdapter<NetworkImage> {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NetworkImageRecyclerViewAdapter.NetworkImageItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_network_image, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((NetworkImageRecyclerViewAdapter.NetworkImageItemViewHolder) holder).bindView(mDataList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.size();
    }

    class NetworkImageItemViewHolder extends BaseRvHolder {

        @BindView(R.id.image_item_network)
        ImageView imageView;
        @BindView(R.id.tv_content)
        TextView content;
        @BindView(R.id.tv_title)
        TextView title;

        View view;

        public NetworkImageItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        @Override
        protected void bindView(NetworkImage imageItem) {

            title.setText(imageItem.getTitle());
            content.setText(imageItem.getContent());
            Glide.with(view).load(imageItem.getUrl()).into(imageView);
        }
    }
}
