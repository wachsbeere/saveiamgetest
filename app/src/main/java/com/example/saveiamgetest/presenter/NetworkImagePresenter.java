package com.example.saveiamgetest.presenter;


import com.example.saveiamgetest.bean.NetworkImage;
import com.example.saveiamgetest.contract.NetworkImageContract;
import com.example.saveiamgetest.model.NetworkImageModel;

import java.util.List;

public class NetworkImagePresenter implements NetworkImageContract.IPresenter {

    private NetworkImageContract.IView mView;
    private NetworkImageContract.IModel mModel;

    public NetworkImagePresenter(NetworkImageContract.IView view) {
        mView = view;
        mModel = new NetworkImageModel(this);
    }

    @Override
    public void getImageByPage(int page) {
        mModel.getImageByPage(page);
    }

    @Override
    public void showImagesInaPaage(List<NetworkImage> imageList) {
        mView.showImagesInaPage(imageList);
    }

    @Override
    public void onError(String msg) {
        mView.onError(msg);
    }
}
