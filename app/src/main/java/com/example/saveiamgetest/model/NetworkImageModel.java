package com.example.saveiamgetest.model;


import android.util.Log;

import com.example.saveiamgetest.bean.NetworkImage;
import com.example.saveiamgetest.bean.NetworkImageDto;
import com.example.saveiamgetest.contract.NetworkImageContract;
import com.example.saveiamgetest.util.GsonUtil;
import com.example.saveiamgetest.util.OkHttpResultCallback;
import com.example.saveiamgetest.util.OkHttpUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

import static com.example.saveiamgetest.constant.Constant.BASE_URL;

public class NetworkImageModel implements NetworkImageContract.IModel {

    private NetworkImageContract.IPresenter mPresenter;

    public NetworkImageModel(NetworkImageContract.IPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void getImageByPage(int page) {

        OkHttpUtil.getInstance().getAsync(BASE_URL + "?page=" + page, new OkHttpResultCallback() {
            @Override
            public void onError(Call call, Exception e) {
                mPresenter.onError("oops..somethings wrong");
            }

            @Override
            public void onResponse(byte[] bytes) {
                try {
                    String response = new String(bytes, "utf-8");

                    if (!response.equals("[]")) {
                        JSONObject jsonObject = new JSONObject(jsonconvert(response));

                        List<NetworkImageDto> networkImageDtoList = GsonUtil.gsonToList(jsonObject.getJSONArray("data").toString(), NetworkImageDto.class);
                        List<NetworkImage> networkImageList = new ArrayList<>();

                        for (NetworkImageDto networkImageDto : networkImageDtoList) {

                            // TODO name
                            networkImageList.add(networkImageDto.transform());
                        }
                        if (networkImageList != null) {
                            mPresenter.showImagesInaPaage(networkImageList);
                        }
                    } else {
                        mPresenter.showImagesInaPaage(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // python mysql返回的数据不是json格式，而且转换的方法复杂，为了节省时间在这里将其转换
    private String jsonconvert(String mysqlresult) {

        String jsonresult = "";
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("{\"status\":\"200 OK\",data:[");

        mysqlresult = mysqlresult.substring(2, mysqlresult.length() - 2);

        String[] strings = mysqlresult.split("], \\[");

        for (int i = 0; i < strings.length; i++) {

            String itemImage = strings[i];

            String[] imageattrs = itemImage.split(", ");

            stringbuilder.append("{");
            for (int j = 0; j < 3; j++) {
                if (j == 0) {
                    stringbuilder.append("\"name\":").append(imageattrs[j]).append(",");
                } else if (j == 1) {
                    stringbuilder.append("\"url\":").append(imageattrs[j]).append(",");
                } else {
                    stringbuilder.append("\"content\":").append(imageattrs[j]);
                }
            }
            stringbuilder.append("}");

            if (i != strings.length - 1) {
                stringbuilder.append(",");
            }
        }
        stringbuilder.append("]}");
        jsonresult = stringbuilder.toString();

        System.out.println("NetworkImageModel jsonconvert2 " + jsonresult);

        return jsonresult;
    }
}


