package com.example.saveiamgetest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME  = "MyImage.db";
    public static final String TABLE_IMAGE = "Image";
    private static final int DATABASE_VERSION  = 1;
    private static MySQLiteHelper dbHelper;
    private final String SQL_CREATE_ENTRIES = "CREATE TABLE IF NOT EXISTS " + TABLE_IMAGE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR2, avatar BLOB)";


    private MySQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private MySQLiteHelper(Context context) {
        this(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static MySQLiteHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new MySQLiteHelper(context);
        }
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
