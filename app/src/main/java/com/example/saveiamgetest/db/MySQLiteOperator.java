package com.example.saveiamgetest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

import com.example.saveiamgetest.bean.ImageItem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MySQLiteOperator {

    private MySQLiteHelper dbhelper;
    private Context context;

    public MySQLiteOperator(Context context) {
        this.context = context;
        this.dbhelper = MySQLiteHelper.getInstance(context);
    }

    public void saveImage(Bitmap bitmap, String imageName) {
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", imageName);
        cv.put("avatar", bitmabToBytes(bitmap));                // 图片转为二进制
        db.insert("Image", null, cv);
        db.close();
    }

    public boolean isPhotoExist(String name) {
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from Image where name=? ", new String[]{name});
        while (cursor.moveToNext()) {
            db.close();
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }

    public List<ImageItem> readImageList() {
        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            db = dbhelper.getReadableDatabase();
            // select * from Orders
            cursor = db.query("Image", new String[]{"_id", "name", "avatar"}, null, null, null, null, null);

            if (cursor.getCount() > 0) {
                List<ImageItem> imageItemList = new ArrayList<>(cursor.getCount());

                System.out.println("MySQLiteOperator readImageList list count ：" + imageItemList.size());

                while (cursor.moveToNext()) {
                    imageItemList.add(parseImageItem(cursor));
                }
                return imageItemList;
            }
        } catch (Exception e) {
            Log.d("exception", "", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
        }

        return null;

    }

    public byte[] bitmabToBytes(Bitmap bitmap) {
        System.out.println("MySQLiteOperator bitmabToBytes 压缩前：" + bitmap.getByteCount() / 1024 / 1024 + "M");

        Bitmap compressbitmap;
        Matrix matrix = new Matrix();
        matrix.setScale(0.3f, 0.3f);
        compressbitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        System.out.println("MySQLiteOperator bitmabToBytes 压缩后：" + compressbitmap.getByteCount() / 1024 / 1024 + "M");

        int size = compressbitmap.getWidth() * compressbitmap.getHeight();
        ByteArrayOutputStream baos = new ByteArrayOutputStream(size);
        try {
            compressbitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
//                bitmap.recycle();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new byte[0];
    }



    private ImageItem parseImageItem(Cursor cursor) {
        ImageItem image = new ImageItem();
        image.setId(cursor.getInt(cursor.getColumnIndex("_id")));
        image.setName(cursor.getString(cursor.getColumnIndex("name")));

        byte[] imageData = cursor.getBlob(cursor.getColumnIndex("avatar"));
        Bitmap imagebitmap = null;
        if (imageData != null) {
            imagebitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
            image.setImage(imagebitmap);
        }
        return image;
    }
}
