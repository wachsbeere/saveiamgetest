package com.example.saveiamgetest.bean;


import com.example.saveiamgetest.listener.Mapper;

public class NetworkImageDto implements Mapper<NetworkImage> {

    private String name;
    private String url;
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public NetworkImage transform() {

        NetworkImage networkImage = new NetworkImage();
        if (name != null) {
            networkImage.setTitle(name + "");
        } else {
            networkImage.setTitle("");
        }

        if (url != null) {
            networkImage.setUrl(url);
        } else {
            networkImage.setUrl("https://static.ws.126.net/www/logo/logo-ipad-icon.png");
        }

        if (content != null) {
            networkImage.setContent(content);
        } else {
            networkImage.setContent("");
        }
        return networkImage;
    }
}

