package com.example.saveiamgetest.bean;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class ImageItem {

    private Bitmap image;
    private String name;
    private int id;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
