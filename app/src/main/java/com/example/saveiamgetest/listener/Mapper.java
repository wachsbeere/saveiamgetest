package com.example.saveiamgetest.listener;

public interface Mapper<T> {
    T transform();
}
