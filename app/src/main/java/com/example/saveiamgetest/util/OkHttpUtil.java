package com.example.saveiamgetest.util;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpUtil {
    private static OkHttpUtil sOkHttpUtil;
    private OkHttpClient.Builder mOkHttpClientBuilder;
    private OkHttpClient mOkHttpClient;
    private Handler mHandler;


    private OkHttpUtil() {
        mOkHttpClientBuilder = new OkHttpClient.Builder();
        mOkHttpClient = mOkHttpClientBuilder.build();
        mHandler = new Handler(Looper.getMainLooper());

    }

    public static OkHttpUtil getInstance() {
        if (sOkHttpUtil == null) {
            synchronized (OkHttpUtil.class) {
                if (sOkHttpUtil == null) {
                    sOkHttpUtil = new OkHttpUtil();
                }
            }
        }
        return sOkHttpUtil;
    }

    public void getAsync(String url, OkHttpResultCallback okHttpResultCallback) {
        Request request = new Request.Builder().url(url).build();
        deliveryResult(okHttpResultCallback, request);
    }

    // 调用call.enqueue，将call加入调度队列，执行完成后在callback中得到结果
    private void deliveryResult(final OkHttpResultCallback okHttpResultCallback, final Request request) {
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                sendFailedCallback(call, e, okHttpResultCallback);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                try {
                    switch (response.code()) {
                        case 200:
                            sendSuccessCallback(response.body().bytes(), okHttpResultCallback);
                            break;
                        case 302:
                            sendSuccessCallback(response.body().bytes(), okHttpResultCallback);
                            break;
                        default:
                            throw new IOException();
                    }
                } catch (IOException e) {
                    sendFailedCallback(call, e, okHttpResultCallback);
                }
            }
        });
    }


    // 调用请求失败对应的回调方法，利用handler.post使得回调方法在UI线程中执行
    private void sendFailedCallback(final Call call, final Exception e, final OkHttpResultCallback callback) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onError(call, e);
                }
            }
        });
    }

    // 调用请求成功对应的回调方法，利用handler.post使得回调方法在UI线程中执行
    private void sendSuccessCallback(final byte[] bytes, final OkHttpResultCallback okHttpResultCallback) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (okHttpResultCallback != null) {
                    okHttpResultCallback.onResponse(bytes);
                }
            }
        });
    }


}