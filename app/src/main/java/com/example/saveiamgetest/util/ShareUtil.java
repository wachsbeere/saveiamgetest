package com.example.saveiamgetest.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import android.provider.MediaStore;
import androidx.fragment.app.FragmentManager;

import com.example.saveiamgetest.app.App;
import com.example.saveiamgetest.fragment.ShareFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShareUtil {

    public static List<ResolveInfo> getShareList(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
        // 调整顺序，把微信、QQ提到前面
        Collections.sort(list, new Comparator<ResolveInfo>() {
            @Override
            public int compare(ResolveInfo resolveInfo, ResolveInfo t1) {
                ActivityInfo activityInfo1 = resolveInfo.activityInfo;
                ActivityInfo activityInfo2 = t1.activityInfo;
                if (activityInfo1.packageName.contains("com.tencent.")
                        && !activityInfo2.packageName.contains("com.tencent.")) {
                    return -1;
                } else if (!activityInfo1.packageName.contains("com.tencent.")
                        && activityInfo2.packageName.contains("com.tencent.")) {
                    return 1;
                }
                return 0;
            }
        });
        return list;
    }

    public static void share(FragmentManager fragmentManager, String title, Uri uri) {
        ShareFragment.getInstance(title, uri).show(fragmentManager, "dialog");
    }

    public static Uri saveBitmap(Bitmap bm, String picName) {
        Uri uri;
        try {
            String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/activity/" + picName + ".jpg";
            File f = new File(dir);
            if (!f.exists()) {
                f.getParentFile().mkdirs();
                f.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 70, out);
            out.flush();
            out.close();
            if (Build.VERSION.SDK_INT >= 24) {
//                uri = FileProvider.getUriForFile(App.getmContext(), "com.example.saveiamgetest.fileprovider", f);
                // android从7.0系统对用户隐私数据的保护，需要临时授权，及微信朋友圈与微信好友多图分享，微信存在的bug，fileprovider方式不能识别为图片形式，故而换作MediaStore形式
                uri = Uri.parse(MediaStore.Images.Media.insertImage(App.getsContentResolver(),
                        f.getAbsolutePath(), f.getName(), null));
            } else {
                uri = Uri.fromFile(f);
            }
            return uri;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

