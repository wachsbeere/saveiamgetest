package com.example.saveiamgetest.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;


public class GsonUtil {
    private static Gson sGson;

    static {
        if (sGson == null) {
            synchronized (GsonUtil.class) {
                if (sGson == null) {
                    sGson = new Gson();
                }
            }
        }
    }

    public static <T> List<T> gsonToList(String response, Class<T> cls) {
        List<T> list = new ArrayList<>();
        if (sGson != null) {
            JsonArray array = new JsonParser().parse(response).getAsJsonArray();
            for (JsonElement element : array) {
                list.add(sGson.fromJson(element, cls));
            }
        }
        return list;
    }
}

